SHELL = /bin/sh
PREVIEW_APP_NAME = vue3app-preview
IMAGE_NAME = dobrohot-app
NETWORK_NAME = dobrohot-network
DOCKER_BIN = $(shell command -v docker 2> /dev/null)
OPEN_RPC_SCHEMA_ENTRY = $(shell pwd)/rpc-types-generator/schema/openrpc.json
GENERATED_TYPES_OUTPUT = $(shell pwd)/src/types

DOCKER_BIN = $(shell command -v docker 2> /dev/null)

build-image:
	$(DOCKER_BIN) build -f ./docker/Dockerfile . -t $(IMAGE_NAME)

init:
	$(DOCKER_BIN) network ls|grep $(NETWORK_NAME) > /dev/null || $(DOCKER_BIN) network create --driver bridge $(NETWORK_NAME)

preview-up:
	@printf "\n   \e[30;43m %s \033[0m\n\n" 'Server started: <http://127.0.0.1:80>'
	$(DOCKER_BIN) run -d -p 80:80 $(IMAGE_NAME)

generate-rpc-types:
	node $(shell pwd)/rpc-types-generator/index.js --schema-entry=$(OPEN_RPC_SCHEMA_ENTRY) > $(GENERATED_TYPES_OUTPUT)/rpc-types.ts

up:
	docker-compose up --detach --remove-orphans nginx

down:
	docker-compose down
