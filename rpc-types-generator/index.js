const OpenRPCTypings = require('@open-rpc/typings').default
const schemaParser = require('@open-rpc/schema-utils-js').parseOpenRPCDocument
const args = require('minimist')(process.argv);

async function gen(entry) {
    const openRPCDocument = await schemaParser(entry)
    const typings = new OpenRPCTypings(openRPCDocument)
    const types = typings.toString('typescript', {
        includeMethodAliasTypings: false,
        includeSchemaTypings: true,
    });
    const attentionMsg = `/* Attention!!! These files were automaticly generated. \n  You must't edit this file */\n\n`;
    const fileContent = `${attentionMsg}${types}`;

    return fileContent;
}

async function run() {
    const types = await gen(args['schema-entry'])
    process.stdout.write(types)
}

run();
