import AppAPI from '@/api';
import type {Emitter} from 'mitt';

import authStorage from '@/services/authStorage';

export default class AuthAPI extends AppAPI {
  constructor(notificationEmitter: Emitter<any>) {
    super(notificationEmitter);
  }

  async auth({ email, password, role }: any): Promise<boolean> {
    const method = `${role}.login`;
    try {
      const { data: { result, error } } = await this.client.call(method, {
        email,
        password,
      })

      if (result) {
        authStorage.id = result.entity.id
        authStorage.token = result.token
        authStorage.role = role
        return true;
      }

      if (error) {
        this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
        return false;
      }

      return false;
    } catch (e) {
      this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });        return false;
      return false;
    }
  }
}
