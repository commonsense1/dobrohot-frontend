import {type IRpcClient, RpcClient} from '@/services/rpcClient';
import type {Emitter} from 'mitt';

const API_BASE_URI = import.meta.env.VITE_API_BASE_URL;

export default class AppAPI {
  public client: IRpcClient;
  protected notificationEmitter: Emitter<any>;
  
  constructor(notificationEmitter: Emitter<any>) {
    const apiEntrypoint = API_BASE_URI + '/rpc'
    this.client = new RpcClient(apiEntrypoint);
    this.notificationEmitter = notificationEmitter;
  }
}
