import AppAPI from '@/api';
import type {Emitter} from 'mitt';
import type {Quest} from '@/types/rpc-types';

export default class QuestApi extends AppAPI {
    constructor(notificationEmitter: Emitter<any>) {
        super(notificationEmitter);
    }

    async list({ offset = 0, limit = 1000 }: any): Promise<Array<any> | boolean> {
        const method = 'quest.list';
        try {
            const { data: { result, error } } = await this.client.call(method, {
                offset,
                limit,
            })

            if (result.length) {
                return result;
            }

            if (!result.length) {
                this.notificationEmitter.emit('notification:push', { msg: 'К сожалению больше нет заданий, возвращайтесь чуть позже! :)' });
            }

            if (error) {
                this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
                return false;
            }

            return false;
        } catch (e) {
            this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
            return false;
        }
    }

    async create(form: any): Promise<boolean> {
        try {
            const params: Quest = {
                organization_id: form.organization_id,
                name: form.name,
                description: form.description,
                img: form.img,
                start_date: form.start_date,
                end_date: form.end_date,
                reward_coins: form.reward_coins,
                reward_exp: form.reward_exp,
                format: form.format,
            };
            const { data: { result, error } } = await this.client.call('quest.add', params);

            if (result) {
                this.notificationEmitter.emit('notification:push', { msg: 'Задание создано!' });
                return true;
            }

            if (error) {
                console.log(error)
                this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
            }
            return false;
        } catch (e) {
            this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
            return false;
        }
    }
    async addRequestFromVolunteer(id: number): Promise<boolean> {
        try {
            const params = {
                quest_id: id,
            };
            const { data: { result, error } } = await this.client.call('quest.prompt.add', params);

            if (result) {
                this.notificationEmitter.emit('notification:push', { msg: 'Заявка отправлена!' });
                return true;
            }

            if (error) {
                console.log(error)
                this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
            }
            return false;
        } catch (e) {
            this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
            return false;
        }
    }
}