import AppAPI from '@/api';
import type {Emitter} from 'mitt';
import type {Organization, Volunteer} from '@/types/rpc-types';

import authStorage from '@/services/authStorage';

export default class RegistrationAPI extends AppAPI {
  constructor(notificationEmitter: Emitter<any>) {
    super(notificationEmitter);
  }
  
  public async registerVolunteer(form: any): Promise<boolean> {
    try {
      const params: Volunteer = {
        last_name: form.lastName,
        first_name: form.firstName,
        email: form.email,
        phone: form.phone,
        password: form.password,
        password_confirmation: form.passwordConfirmation
      };
      const { data: { result, error } } = await this.client.call('volunteer.register', params);
      
      if (result) {
        this.notificationEmitter.emit('notification:push', { msg: 'Вы успешно зарегистрированы!' });
        authStorage.id = result.id
        authStorage.token = result.token;
        authStorage.role = 'volunteer';
        return true;
      }
      
      if (error) {
        this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
      }
      return false;
    } catch (e) {
      this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
      return false;
    }
  }
  
  public async registerOrganization(form: any): Promise<boolean> {
    try {
      const params: Organization = {
        name: form.lastName,
        email: form.email,
        phone: form.phone,
        password: form.password,
        password_confirmation: form.passwordConfirmation
      };
      const { data: { result, error } } = await this.client.call('organization.register', params);
      
      if (result) {
        this.notificationEmitter.emit('notification:push', { msg: 'Вы успешно зарегистрированы!' });
        authStorage.id = result.id
        authStorage.token = result.token;
        authStorage.role = 'organization';
        return true;
      }
      
      if (error) {
        this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
      }
      return false;
    } catch (e) {
      this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
      return false;
    }
  }
}
