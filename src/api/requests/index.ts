import AppAPI from '@/api';
import type {Emitter} from 'mitt';

export default class RequestsApi extends AppAPI {
  constructor(notificationEmitter: Emitter<any>) {
    super(notificationEmitter);
  }

  async listByOrganization({ offset = 0, limit = 1000, organizationId }: any) {
    const method = 'quest.prompt.listByOrganization';
    try {
      const { data: { result, error } } = await this.client.call(method, {
        offset,
        limit,
        organization_id: organizationId,
      })

      if (result.length) {
        return result;
      }

      if (!result.length) {
        this.notificationEmitter.emit('notification:push', { msg: 'К сожалению больше нет заявок, возвращайтесь чуть позже! :)' });
      }

      if (error) {
        this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
        return false;
      }

      return false;
    } catch (e) {
      this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
      return false;
    }
  }

  async listByVolunteer({ offset = 0, limit = 1000, volunteerId }: any) {
    const method = 'quest.prompt.listByVolunteer';
    try {
      const { data: { result, error } } = await this.client.call(method, {
        offset,
        limit,
        volunteer_id: volunteerId,
      })
      if (result.length) {
        return result;
      }

      if (!result.length) {
        this.notificationEmitter.emit('notification:push', { msg: 'К сожалению больше нет заявок, возвращайтесь чуть позже! :)' });
      }

      if (error) {
        console.log(error)
        this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
        return false;
      }

      return false;
    } catch (e) {
      console.log(e)
      this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
      return false;
    }
  }

  async approve({ promptId }: any) {
    try {
      const method = 'quest.prompt.approve';

      const { data: { result, error } } = await this.client.call(method, {
        prompt_id: promptId,
      })

      if (result) {
        return true;
      }

      if (error) {
        this.notificationEmitter.emit('notification:push', { msg: 'Что-то пошло не так' });
        return false;
      }

      return false;
    } catch (e) {
      this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
      return false;
    }
  }
}
