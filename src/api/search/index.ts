import AppAPI from '@/api';
import type {Emitter} from 'mitt';

export default class SearchApi extends AppAPI {
    constructor(notificationEmitter: Emitter<any>) {
        super(notificationEmitter);
    }


    async getVolunteers({ offset = 0, limit = 1000 }: any) {
        const method = 'volunteer.list';
        try {
            const { data: { result, error } } = await this.client.call(method, {
                offset,
                limit,
            })
            if (result.length) {
                return result;
            }

            if (!result.length) {
                this.notificationEmitter.emit('notification:push', { msg: 'К сожалению больше нет заявок, возвращайтесь чуть позже! :)' });
            }

            if (error) {
                console.log(error)
                this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
                return false;
            }

            return false;
        } catch (e) {
            console.log(e)
            this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
            return false;
        }
    }

    async getQuests({ offset = 0, limit = 1000 }: any) {
        const method = 'quest.list';
        try {
            const { data: { result, error } } = await this.client.call(method, {
                offset,
                limit,
            })
            if (result.length) {
                return result;
            }

            if (!result.length) {
                this.notificationEmitter.emit('notification:push', { msg: 'К сожалению больше нет заявок, возвращайтесь чуть позже! :)' });
            }

            if (error) {
                console.log(error)
                this.notificationEmitter.emit('notification:push', { msg: 'Произошла ошибка' });
                return false;
            }

            return false;
        } catch (e) {
            console.log(e)
            this.notificationEmitter.emit('notification:push', { msg: 'Произошла непредвиденная ошибка' });
            return false;
        }
    }
}