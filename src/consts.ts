export type TRole = 'volunteer' | 'organization';

export const roles: TRole[] = ['volunteer', 'organization'];
