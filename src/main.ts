import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import { getEmitter } from '@/services/emitter';

const app = createApp(App)
export const emitter = getEmitter()

app.use(createPinia())
app.use(router)
app.config.globalProperties.emitter = emitter

app.mount('#app')
