export const mockQuest = {
  id: '123',
  organizationId: '123',
  name: 'Некоторое задание',
  description: '123',
  startDate: '23.06.2022',
  endDate: '25.06.2022',
  rewardCoins: 10,
  rewardExp: 1000,
}

export const volunteerProfileMock = {
  id: '123',
  name: 'Иванов Иван Вячеславович',
  description: 'lorem',
  year: 34,
}
