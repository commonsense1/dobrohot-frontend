import type {RouteRecordRaw} from 'vue-router';

import AuthPage from '@/pages/auth/AuthPage.vue';

export function createAuthRouters(): RouteRecordRaw[] {
  return [
    {
      path: '/auth',
      name: 'auth',
      component: AuthPage,
    },
  ]
}
