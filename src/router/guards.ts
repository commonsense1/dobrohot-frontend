import authStorage from '@/services/authStorage';
import type {RouteLocation} from 'vue-router';


export function requireAuth(to: RouteLocation, from: RouteLocation, next: any) {
  if (!authStorage.isLoggedIn) {
    alert('Вы должны быть зарегистрированы для доступа к этой странице.')
    next({
      name: 'home'
    });
  } else {
    next()
  }
}

export function mustBeVolunteer(to: RouteLocation, from: RouteLocation, next: any) {
  const isVolunteer = (authStorage.role === 'volunteer')
  console.log('authStorage')
  console.log(authStorage)
  if (!isVolunteer) {
    alert('Вы должны быть зарегистрированы волонтером, для просмотра этой страницы. Зарегистрируйтесь и возвращайтесь! :) ')
    next({
      name: 'home'
    });
  } else {
    next()
  }
}

export function checkRole(to: RouteLocation, from: RouteLocation, next: any) {
  if (authStorage.role && to.name === 'home') {
    next({
      name: `${authStorage.role}-start`
    })
  }
  next()
}
