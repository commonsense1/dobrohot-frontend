import {createRouter,createWebHistory} from 'vue-router'

import {createRegistrationRouters} from '@/router/registration';
import {createAuthRouters} from '@/router/auth';
import {createVolunteerRouters} from '@/router/volunteer';
import {createOrganizationRouters} from '@/router/organization';
import {createProfileRouters} from '@/router/profile';

import HomePage from '@/pages/HomePage.vue';
import UiPage from '@/pages/UiPage.vue';

import {checkRole} from '@/router/guards';
import {createSearchRouters} from "@/router/search";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/ui-page',
      name: 'ui-page',
      component: UiPage
    },
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    ...createRegistrationRouters(),
    ...createAuthRouters(),
    ...createVolunteerRouters(),
    ...createOrganizationRouters(),
    ...createProfileRouters(),
    ...createSearchRouters(),
  ]
})

router.beforeResolve(checkRole)

export default router
