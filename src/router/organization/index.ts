import type {RouteRecordRaw} from 'vue-router';

import OrganizationStartPage from '@/pages/organization/OrganizationStartPage.vue';
import OrganizationRecommendations from '@/pages/organization/OrganizationRecommendations.vue';
import OrganizationRequestPage from '@/pages/organization/requests/OrganizationRequestPage.vue';
import OrganizationRequestsPage from '@/pages/organization/requests/OrganizationRequestsPage.vue';
import OrganizationQuestAddPage from '@/pages/organization/quest/OrganizationQuestAddPage.vue';

import {requireAuth} from '@/router/guards';
import OrganizationQuestionnaire from '@/pages/profile/OrganizationQuestionnairePage.vue';
import OrganizationProfileViewPage from '@/pages/profile/OrganizationProfileViewPage.vue';

export function createOrganizationRouters(): RouteRecordRaw[] {
  return [
    {
      path: '/organization',
      name: 'organization-start',
      component: OrganizationStartPage,
      meta: { permissionRole: 'organization' },
      beforeEnter: requireAuth
    },
    {
      path: '/organization/requests',
      name: 'organization-requests',
      component: OrganizationRequestsPage,
      meta: { permissionRole: 'organization' },
      beforeEnter: requireAuth
    },
    {
      path: '/organization/requests/:id',
      name: 'organization-request',
      component: OrganizationRequestPage,
      meta: { permissionRole: 'organization' },
      beforeEnter: requireAuth
    },
    {
      path: '/organization/quest/create',
      name: 'organization-quest',
      component: OrganizationQuestAddPage,
      meta: { permissionRole: 'volunteer' },
      beforeEnter: requireAuth
    },
    {
      path: '/organization/recommendations',
      name: 'organization-recommendations',
      component: OrganizationRecommendations,
      beforeEnter: [requireAuth]
    },
    {
      path: '/organization/profile/edit',
      name: 'register-questionnaire',
      component: OrganizationQuestionnaire
    },
    {
      path: '/organization/profile',
      name: 'organization-profile-view-self',
      component: OrganizationProfileViewPage
    },
  ]
}


