import OrganizationQuestionnaire from '@/pages/profile/OrganizationQuestionnairePage.vue';
import OrganizationProfileViewPage from '@/pages/profile/OrganizationProfileViewPage.vue';
import GuestProfileOrganizationViewPage from '@/pages/profile/GuestProfileOrganizationViewPage.vue';
import VolunteerQuestionnaire from '@/pages/profile/VolunteerQuestionnaire.vue';
import VolunteerProfileViewPage from '@/pages/profile/VolunteerProfileViewPage.vue';
import GuestProfileVolunteerViewPage from '@/pages/profile/GuestProfileVolunteerViewPage.vue';

import type {RouteRecordRaw} from 'vue-router';

export function createProfileRouters(): RouteRecordRaw[] {
    return [
        {
            path: '/profile/volunteer-view',
            name: 'volunteer-profile-view',
            component: GuestProfileVolunteerViewPage,
        },
        {
            path: '/profile/organization-view',
            name: 'organization-profile-view',
            component: GuestProfileOrganizationViewPage
        },
    ]
}
