import RegisterVolunteerPage from '@/pages/registration/RegisterVolunteerPage.vue';
import RegisterOrganizationPage from '@/pages/registration/RegisterOrganizationPage.vue';

import type {RouteRecordRaw} from 'vue-router';

export function createRegistrationRouters(): RouteRecordRaw[] {
  return [
    {
      path: '/register/volunteer',
      name: 'register-volunteer',
      component: RegisterVolunteerPage,
    },
    {
      path: '/register/organization',
      name: 'register-organization',
      component: RegisterOrganizationPage,
    },
  ]
}


