import type {RouteRecordRaw} from 'vue-router';

import VolunteersSearchPage from '@/pages/search/VolunteersSearchPage.vue';
import QuestsSearchPage from '@/pages/search/QuestsSearchPage.vue';

export function createSearchRouters(): RouteRecordRaw[] {
    return [
        {
            path: '/search/volunteers',
            name: 'volunteers-search',
            component: VolunteersSearchPage,
        },
        {
            path: '/search/quests',
            name: 'quests-search',
            component: QuestsSearchPage,
        },

    ]
}


