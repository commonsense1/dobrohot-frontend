import type {RouteRecordRaw} from 'vue-router';

import VolunteerStartPage from '@/pages/volunteer/VolunteerStartPage.vue';
import VolunteerRecommendations from '@/pages/volunteer/VolunteerRecommendations.vue';
import VolunteerChildPage from '@/pages/volunteer/VolunteerChildPage.vue';
import {mustBeVolunteer, requireAuth} from '@/router/guards';
import VolunteerProfileViewPage from '@/pages/profile/VolunteerProfileViewPage.vue';
import QuestsPage from '@/pages/quests/QuestsPage.vue';
import QuestPage from '@/pages/quests/QuestPage.vue';
import VolunteerQuestionnaire from '@/pages/profile/VolunteerQuestionnaire.vue';
import VolunteerRequestsPage from '@/pages/volunteer/requests/VolunteerRequestsPage.vue';

export function createVolunteerRouters(): RouteRecordRaw[] {
  return [
    {
      path: '/volunteer',
      name: 'volunteer-start',
      component: VolunteerStartPage,
      meta: { permissionRole: 'volunteer' },
      beforeEnter: [requireAuth]
    },
    {
      path: '/volunteer/profile',
      name: 'volunteer-profile-view-self',
      component: VolunteerProfileViewPage,
      meta: { permissionRole: 'profile' },
      beforeEnter: [requireAuth]
    },
    {
      path: '/volunteer/child',
      name: 'volunteer-child',
      component: VolunteerChildPage,
      meta: { permissionRole: 'volunteer' },
      beforeEnter: [requireAuth]
    },
    {
      path: '/volunteer/quests/',
      name: 'volunteer-quests',
      component: QuestsPage,
      beforeEnter: [requireAuth, mustBeVolunteer]
    },
    {
      path: '/volunteer/quests/:id',
      name: 'volunteer-quest',
      component: QuestPage,
      beforeEnter: [requireAuth, mustBeVolunteer]
    },
    {
      path: '/volunteer/recommendations',
      name: 'volunteer-recommendations',
      component: VolunteerRecommendations,
      beforeEnter: [requireAuth, mustBeVolunteer]
    },
    {
      path: '/volunteer/profile/edit',
      name: 'volunteer-questionnaire',
      component: VolunteerQuestionnaire,
    },
    {
      path: '/volunteer/profile',
      name: 'volunteer-profile-view-self',
      component: VolunteerProfileViewPage,
    },
    {
      path: '/volunteer/profile/requests',
      name: 'volunteer-profile-view-requsets',
      component: VolunteerRequestsPage,
    },
  ]
}


