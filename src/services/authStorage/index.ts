import type {TRole} from '@/consts';

interface IAuthStorage {
  token: string
  isLoggedIn: boolean
  
  logout: () => void
}

const TOKEN_KEY = 'jwt';
const ROLE_KEY = 'role';
const ID_KEY = 'id';

class AuthStorage implements IAuthStorage {
  private readonly tokenKey = TOKEN_KEY;
  private readonly roleKey = ROLE_KEY;
  private readonly idKey = ID_KEY;
  private memoryStorage: { [key in string]?: string } = {};
  
  public get token(): string {
    if (localStorage) {
      return localStorage.getItem(this.tokenKey) || '';
    }
    return this.memoryStorage[this.tokenKey] || '';
  }
  
  public set token(newToken: string) {
    if (localStorage) {
      localStorage.setItem(this.tokenKey, newToken);
    } else {
      this.memoryStorage[this.tokenKey] = newToken;
    }
  }
  
  public get role(): TRole {
    if (localStorage) {
      return localStorage.getItem(this.roleKey) as TRole || '';
    }
    return this.memoryStorage[this.roleKey] as TRole || '';
  }
  
  public set role(role: TRole) {
    if (localStorage) {
      localStorage.setItem(this.roleKey, role);
    } else {
      this.memoryStorage[this.roleKey] = role;
    }
  }

  public get id(): TRole {
    if (localStorage) {
      return localStorage.getItem(this.idKey) as TRole || '';
    }
    return this.memoryStorage[this.idKey] as TRole || '';
  }

  public set id(role: TRole) {
    if (localStorage) {
      localStorage.setItem(this.idKey, role);
    } else {
      this.memoryStorage[this.idKey] = role;
    }
  }

  public get isLoggedIn(): boolean {
    return Boolean(this.token);
  }
  
  public logout(): void {
    localStorage.removeItem(this.tokenKey);
    localStorage.removeItem(this.idKey);
  }
}

const authStorage = new AuthStorage();

export default authStorage;
