import mitt, {type Emitter} from 'mitt';

let emitter: null | Emitter<any> = null;

export function getEmitter() {
  if (emitter) {
    return emitter;
  } else {
    emitter = mitt();
    return emitter;
  }
}
