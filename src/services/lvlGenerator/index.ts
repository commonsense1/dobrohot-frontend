const EXP_TO_LVL_MAP = {
  1: [0, 500],
  2: [500, 1000],
  3: [1000, 1500],
  4: [1500, 2000],
}

export default function (exp: number): number {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const idx = Object.values(EXP_TO_LVL_MAP).findIndex(([from, to]: number[]) => exp >= from && exp <= to)
  return idx + 1;
}
