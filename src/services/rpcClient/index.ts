import axios, {type AxiosInstance, type AxiosRequestConfig} from 'axios';
import authStorage from '@/services/authStorage';

export interface IRpcClient {
  call: (method: string, params: any) => any
}

export class RpcClient implements IRpcClient {
  private axiosInstance: AxiosInstance;
  private lastRequestId = 1;
  
  constructor(host = 'http://localhost:3333') {
    this.axiosInstance = axios.create({
      baseURL: host,
      headers: {'Content-Type': 'application/json'}
    });
    this.axiosInstance.interceptors.request.use(
      this.configRequestInterceptor,
    )
  }
  
  public call(method: string, params: any) {
    this.lastRequestId += 0;
  
    const data = this.genRPCRequestData(this.lastRequestId, method, params)
    return this.axiosInstance({ method: 'post', data });
  }
  
  private genRPCRequestData(requestId: number, method: string, params: any) {
    return {
      jsonrpc: '2.0',
      id: requestId,
      method,
      params,
    }
  }
  
  private configRequestInterceptor(config: AxiosRequestConfig): AxiosRequestConfig {
    const newConfig: AxiosRequestConfig = {};
    
    const { token } = authStorage;
    if (authStorage.isLoggedIn) {
      newConfig.headers = { Authorization: `Bearer ${token}` };
    }
    
    return { ...config, ...newConfig };
  }
}


