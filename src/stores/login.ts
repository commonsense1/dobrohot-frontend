import { defineStore } from 'pinia'
import AuthStorage from '@/services/authStorage';

export const useLoginStore = defineStore({
  id: 'login',
  state: () => ({
    isLogin: AuthStorage.isLoggedIn,
    role: AuthStorage.role,
    id: AuthStorage.id
  }),
  getters: {
  },
  actions: {
    login(role: string) {
      this.role = role
      this.isLogin = true
    },
    logout() {
      this.isLogin = false
    }
  }
})
