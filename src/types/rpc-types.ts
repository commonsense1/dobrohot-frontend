/* Attention!!! These files were automaticly generated. 
  You must't edit this file */

export type OrganizationPhoneStr = string;
export type OrganizationPhoneNull = null;
export type VolunteerPhoneStr = string;
export type VolunteerPhoneNull = null;
export type VolunteerGenderStr = string;
export type VolunteerGenderNull = null;
export type VolunteerBirthdayStr = string;
export type VolunteerBirthdayNull = null;
export type VolunteerVkProfileStr = string;
export type VolunteerVkProfileNull = null;
export type VolunteerTelegramProfileStr = string;
export type VolunteerTelegramProfileNull = null;
export type VolunteerJobPlaceStr = string;
export type VolunteerJobPlaceNull = null;
export type VolunteerEducationStr = string;
export type VolunteerEducationNull = null;
export type Integer2AHOqbcQ = number;
export interface ObjectDDBDaJEg { [key: string]: any; }
export type Token = string;
export type Id = number;
/**
 *
 * Organization name
 *
 */
export type OrganizationName = string;
/**
 *
 * Organization email
 *
 */
export type OrganizationEmail = string;
export type OrganizationPhone = OrganizationPhoneStr | OrganizationPhoneNull;
/**
 *
 * Volunteer last name
 *
 */
export type VolunteerLastName = string;
/**
 *
 * Volunteer first name
 *
 */
export type VolunteerFirstName = string;
/**
 *
 * Volunteer email
 *
 */
export type VolunteerEmail = string;
export type VolunteerPhone = VolunteerPhoneStr | VolunteerPhoneNull;
export type LastName = string;
export type FirstName = string;
export type Email = string;
export type String = string;
export type CreatedAt = string;
export type UpdatedAt = string;
export type Birthday = VolunteerBirthdayStr | VolunteerBirthdayNull;
export type Gender = VolunteerGenderStr | VolunteerGenderNull;
export type VkProfile = VolunteerVkProfileStr | VolunteerVkProfileNull;
export type TelegramProfile = VolunteerTelegramProfileStr | VolunteerTelegramProfileNull;
export type Education = VolunteerEducationStr | VolunteerEducationNull;
export type JobPlace = VolunteerJobPlaceStr | VolunteerJobPlaceNull;
export type AnyXBQQq582 = any;
export type AnyORSkGVVR = any;
export type AnyGm4NQxrm = any;
export type AnyG7HS9CHa = any;
export type AnyKEB56Qvw = any;
export type AnyHWwDAHK8 = any;
export type Any4ATH69V1 = any;
export type AnyAOpKW1Mj = any;
export type AnyPIsZximY = any;
export type AnyBg4Oou4S = any;
export type QuestOrganizationId = number;
export type QuestName = string;
export type QuestDescStr = string;
export type QuestDescNull = null;
export type OneOfQuestDescNullQuestDescStrIZEiMSOV = QuestDescStr | QuestDescNull;
export type QuestImgStr = string;
export type QuestImgNull = null;
export type OneOfQuestImgNullQuestImgStrJ7F9Wl45 = QuestImgStr | QuestImgNull;
export type QuestStartDate = string;
export type QuestEndDate = string;
export type QuestRewardCoins = number;
export type QuestRewardExp = number;
export type QuestFormat = "online" | "offline";
export interface ObjectGFZZHOBD { [key: string]: any; }
export interface Quest {
  id?: Id;
  organization_id?: AnyXBQQq582;
  name?: AnyORSkGVVR;
  description?: AnyGm4NQxrm;
  img?: AnyG7HS9CHa;
  start_date?: AnyKEB56Qvw;
  end_date?: AnyHWwDAHK8;
  reward_coins?: Any4ATH69V1;
  reward_exp?: AnyAOpKW1Mj;
  format?: AnyPIsZximY;
  tags?: AnyBg4Oou4S;
}
export type TagName = string;
export type TagColor = string;
/**
 *
 * Volunteer password
 *
 */
export type VolunteerPassword = string;
/**
 *
 * Organization password
 *
 */
export type OrganizationPassword = string;
export type Phone = OrganizationPhoneStr | OrganizationPhoneNull;
/**
 *
 * Organization password confirmation
 *
 */
export type OrganizationPasswordConfirmation = string;
/**
 *
 * Volunteer password confirmation
 *
 */
export type VolunteerPasswordConfirmation = string;
export type Skills = Integer2AHOqbcQ[];
export type OrganizationId = number;
export type QuestDescription = string;
export type QuestImg = string;
export type QuestExp = number;
export type QuestTags = ObjectDDBDaJEg[];
export type QuestLimit = number;
export type QuestOffset = number;
export interface VolunteerToken {
  token?: Token;
  [k: string]: any;
}
export interface OrganizationToken {
  token?: Token;
  [k: string]: any;
}
export interface Organization {
  id?: Id;
  name?: OrganizationName;
  email?: OrganizationEmail;
  phone?: OrganizationPhone;
  token?: Token;
}
export interface Volunteer {
  id?: Id;
  last_name?: VolunteerLastName;
  first_name?: VolunteerFirstName;
  email?: VolunteerEmail;
  phone?: VolunteerPhone;
  token?: Token;
}
export interface VolunteerProfile {
  id: Id;
  last_name: LastName;
  first_name: FirstName;
  email: Email;
  phone?: String;
  created_at?: CreatedAt;
  updated_at?: UpdatedAt;
  birthday?: Birthday;
  gender?: Gender;
  vk_profile?: VkProfile;
  telegram_profile?: TelegramProfile;
  education?: Education;
  job_place?: JobPlace;
}
export type QuestList = Quest[];
export interface TagList {
  id: Id;
  name: TagName;
  color?: TagColor;
  created_at?: CreatedAt;
  updated_at?: UpdatedAt;
}
/**
 *
 * Generated! Represents an alias to any of the provided schemas
 *
 */
export type AnyOfVolunteerEmailVolunteerPasswordOrganizationEmailOrganizationPasswordOrganizationNameOrganizationEmailPhoneOrganizationPasswordOrganizationPasswordConfirmationVolunteerLastNameVolunteerFirstNameVolunteerEmailPhoneVolunteerPasswordVolunteerPasswordConfirmationVolunteerLastNameVolunteerFirstNameVolunteerEmailPhoneGenderBirthdayVkProfileTelegramProfileJobPlaceEducationSkillsOrganizationIdQuestNameQuestDescriptionQuestImgQuestStartDateQuestEndDateQuestRewardCoinsQuestExpQuestFormatQuestTagsQuestLimitQuestOffsetVolunteerTokenOrganizationTokenOrganizationVolunteerVolunteerProfileVolunteerProfileQuestQuestListTagList = VolunteerEmail | VolunteerPassword | OrganizationEmail | OrganizationPassword | OrganizationName | Phone | OrganizationPasswordConfirmation | VolunteerLastName | VolunteerFirstName | VolunteerPasswordConfirmation | Gender | Birthday | VkProfile | TelegramProfile | JobPlace | Education | Skills | OrganizationId | QuestName | QuestDescription | QuestImg | QuestStartDate | QuestEndDate | QuestRewardCoins | QuestExp | QuestFormat | QuestTags | QuestLimit | QuestOffset | VolunteerToken | OrganizationToken | Organization | Volunteer | VolunteerProfile | Quest | QuestList | TagList;